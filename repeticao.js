// comentario de 1 linha

/*
 
  bloco de codigo
  */
 console.log("repetição while")
  let numero = 0;

  while(numero <= 10){

    if(numero %2 == 0 ){
      console.log(`Valor numero: ${numero} é PAR`);
    }else{
      console.log(`Valor numero: ${numero}`);
    }
    numero++;
  }

  console.log("repetição Do/while")

  let numero1 = 0;

  do{
    console.log(`valor nr: ${numero1}`)
    numero1++;
  }while(numero1 <=10);

  console.log("repetição FOR");
  for(let numero2 = 0; numero2 <= 10; numero2++){
    console.log(`valor nr: ${numero2}`);
  }